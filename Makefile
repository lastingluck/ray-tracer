# The compiler to use
CC=g++

# Folders that the source files are stored in and where the object files should be stored
OBJ=obj
SRC=src

# FILES contains the list of all of the files that are part of the project
FILES=main.cpp parse.cpp raytrace.cpp image.cpp pixel.cpp EasyBMP.cpp
SOURCES=$(addprefix $(SRC)/, $(FILES))
OBJECTS=$(addprefix $(OBJ)/, $(FILES:.cpp=.o))
# Name of the executable
EXECUTABLE=raytrace

# Use std=c++11 if the system is linux, else use older lstdc++ (for clang)
UNAME := $(shell uname)
ifeq ($(UNAME), Linux)
LFLAGS=-std=c++11
else
LFLAGS=-lstdc++
endif

CFLAGS=-c -Wall
DFLAGS=-g
ifdef DEBUG
	DFLAGS+= -DDEBUG=1 
endif
ifdef PARALLEL
	DFLAGS+= -fopenmp -DPARALLEL=1
endif

all: $(SOURCES) $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(DFLAGS) $(LFLAGS) $(OBJECTS) -o $@

$(OBJ)/%.o: $(SRC)/%.cpp
	$(CC) $(CFLAGS) $(DFLAGS) $(LFLAGS) $< -o $@

clean:
	rm -rf $(OBJ)/*.o $(EXECUTABLE)
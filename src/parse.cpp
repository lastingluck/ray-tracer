#include "../include/parse.hpp"
#include <cstdio>
#include <cstring>
#include <fstream>
#include <stdexcept>

#define DEGTORAD 0.01745329251

/*======================== X-tests ========================*/

#define AXISTEST_X01(a, b, fa, fb)			   \
	p0 = a*v0.y - b*v0.z;			       	   \
	p2 = a*v2.y - b*v2.z;			       	   \
        if(p0<p2) {fmin=p0; fmax=p2;} else {fmin=p2; fmax=p0;} \
	rad = fa * boxhalfsize.y + fb * boxhalfsize.z;   \
	if(fmin>rad || fmax<-rad) return false;

#define AXISTEST_X2(a, b, fa, fb)			   \
	p0 = a*v0.y - b*v0.z;			           \
	p1 = a*v1.y - b*v1.z;			       	   \
        if(p0<p1) {fmin=p0; fmax=p1;} else {fmin=p1; fmax=p0;} \
	rad = fa * boxhalfsize.y + fb * boxhalfsize.z;   \
	if(fmin>rad || fmax<-rad) return false;

/*======================== Y-tests ========================*/

#define AXISTEST_Y02(a, b, fa, fb)			   \
	p0 = -a*v0.x + b*v0.z;		      	   \
	p2 = -a*v2.x + b*v2.z;	       	       	   \
        if(p0<p2) {fmin=p0; fmax=p2;} else {fmin=p2; fmax=p0;} \
	rad = fa * boxhalfsize.x + fb * boxhalfsize.z;   \
	if(fmin>rad || fmax<-rad) return false;

#define AXISTEST_Y1(a, b, fa, fb)			   \
	p0 = -a*v0.x + b*v0.z;		      	   \
	p1 = -a*v1.x + b*v1.z;	     	       	   \
        if(p0<p1) {fmin=p0; fmax=p1;} else {fmin=p1; fmax=p0;} \
	rad = fa * boxhalfsize.x + fb * boxhalfsize.z;   \
	if(fmin>rad || fmax<-rad) return false;

/*======================== Z-tests ========================*/

#define AXISTEST_Z12(a, b, fa, fb)			   \
	p1 = a*v1.x - b*v1.y;			           \
	p2 = a*v2.x - b*v2.y;			       	   \
        if(p2<p1) {fmin=p2; fmax=p1;} else {fmin=p1; fmax=p2;} \
	rad = fa * boxhalfsize.x + fb * boxhalfsize.y;   \
	if(fmin>rad || fmax<-rad) return false;

#define AXISTEST_Z0(a, b, fa, fb)			   \
	p0 = a*v0.x - b*v0.y;				   \
	p1 = a*v1.x - b*v1.y;			           \
    if(p0<p1) {fmin=p0; fmax=p1;} else {fmin=p1; fmax=p0;} \
	rad = fa * boxhalfsize.x + fb * boxhalfsize.y;   \
	if(fmin>rad || fmax<-rad) return false;

/********************
 * Scene
 ********************/
Scene::Scene() {
    init();
}

Scene::Scene(enum ProjType view) {
    init();
    proj = view;
}

void Scene::init() {
    //Camera
    camera = Camera(Vector(), Vector(0, 0, 1), Vector(0, 1, 0), 45.0*DEGTORAD);
    //Image
    img = rt::Image("raytraced.bmp", 640, 480);
    //Light
    ambient = Light(Vector(0, 0, 0));
    //Misc
    bgColor = Vector(0, 0, 0);
    maxDepth = 5;
    proj = PERSP;
    //BVH
    sThresh = 500;
    sDepth = 5;
    useSpStruct = false;
    //Super sampling
    sampleNum = 1;
    #ifdef DEBUG
    printf("Setup Done\n");
    #endif
}

int Scene::parseScene(char* file) {
    char trash[256];
    std::ifstream scn;
    scn.open(file);
    if(scn.fail()) {
        printf("File could not be opened: %s\n", file);
        return -1;
    }
    Material curMat(Vector::zero(), Vector::one(), Vector::zero(), Vector::zero(), 5, 1);
    std::string str;
    while(scn >> str) {
        if(str == "" || str[0] == '#') {
            scn.getline(trash, 256);
            continue;
        }
        else if(str == "camera") {
            float x, y, z, dx, dy, dz, ux, uy, uz, ha;
            scn >> x >> y >> z >> dx >> dy >> dz >> ux >> uy >> uz >> ha;
            camera.setPosition(Vector(x, y, z));
            camera.setDirection(Vector(dx, dy, dz));
            camera.setUpDirection(Vector(ux, uy, uz));
            camera.setHalfFOV(ha*DEGTORAD);
        }
        else if(str == "film_resolution") {
            int w, h;
            scn >> w >> h;
            img.setWidth(w);
            img.setHeight(h);
        }
        else if(str == "output_image") {
            std::string n;
            scn >> n;
            img.setFileName(n);
        }
        else if(str == "sphere") {
            //std::string tmp;
            //scn >> tmp;
            float x, y, z, r;
            scn >> x >> y >> z >> r;
            //printf("Sphere at (%s)\n", tmp.c_str());
            addObject(new Sphere(Vector(x, y, z), r, curMat));
        }
        else if(str == "background") {
            float r, g, b;
            scn >> r >> g >> b;
            setBackground(Vector(r, g, b));
        }
        else if(str == "material") {
            float ar, ag, ab, dr, dg, db, sr, sg, sb, ns, tr, tg, tb, ior;
            scn >> ar >> ag >> ab >> dr >> dg >> db >> sr >> sg >> sb >> ns 
                >> tr >> tg >> tb >> ior;
            curMat.setAmbient(Vector(ar, ag, ab));
            curMat.setDiffuse(Vector(dr, dg, db));
            curMat.setSpecular(Vector(sr, sg, sb));
            curMat.setTransmissive(Vector(tr, tg, tb));
            curMat.setCosPower(ns);
            curMat.setIndexRefract(ior);
        }
        else if(str == "directional_light") {
            float r, g, b, x, y, z;
            scn >> r >> g >> b >> x >> y >> z;
            addLight(Light(Vector(r, g, b), Vector(x, y, z), DIRECTIONAL));
        }
        else if(str == "point_light") {
            float r, g, b, x, y, z;
            scn >> r >> g >> b >> x >> y >> z;
            addLight(Light(Vector(r, g, b), Vector(x, y, z), POINT));
        }
        else if(str == "spot_light") {
            float r, g, b, px, py, pz, dx, dy, dz, angle1, angle2;
            scn >> r >> g >> b >> px >> py >> pz >> dx >> dy >> dz >> angle1 >> angle2;
            addLight(Light(Vector(r, g, b), Vector(px, py, pz), Vector(dx, dy, dz), angle1, angle2));
        }
        else if(str == "ambient_light") {
            float r, g, b;
            scn >> r >> g >> b;
            setAmbientLight(Light(Vector(r, g, b)));
        }
        else if(str == "max_depth") {
            int n;
            scn >> n;
            setMaxDepth(n);
        }
        else if(str == "ray_type") {
            std::string type;
            scn >> type;
            if(type == "perspective") {
                setProjType(PERSP);
            }
            else if(type == "orthographic") {
                setProjType(ORTHO);
            }
            else {
                printf("Unknown Projection Type: %s\n", type.c_str());
				printf("Defaulting to Perspective\n");
                setProjType(PERSP);
            }
        }
        else if(str == "vertex") {
            float x, y, z;
            scn >> x >> y >> z;
            addVertex(Vector(x, y, z));
        }
        else if(str == "normal") {
            float x, y, z;
            scn >> x >> y >> z;
            addNormal(Vector(x, y, z).norm());
        }
        else if(str == "triangle") {
            int v1, v2, v3;
            scn >> v1 >> v2 >> v3;
            std::vector<Vector> vl = getVertices();
            int vn = vl.size();
            if(v1 >= vn || v1 < 0) {
                printf("Error: Specified vertex (%d) in triangle (%d) does not exist\n", v1, (int)getTriangles().size());
                return -1;
            }
            if(v2 >= vn || v2 < 0) {
                printf("Error: Specified vertex (%d) in triangle (%d) does not exist\n", v2, (int)getTriangles().size());
                return -1;
            }
            if(v3 >= vn || v3 < 0) {
                printf("Error: Specified vertex (%d) in triangle (%d) does not exist\n", v3, (int)getTriangles().size());
                return -1;
            }
            addObject(new Triangle(vl[v1], vl[v2], vl[v3], curMat));
        }
        else if(str == "normal_triangle") {
            int v1, v2, v3, n1, n2, n3;
            scn >> v1 >> v2 >> v3 >> n1 >> n2 >> n3;
            std::vector<Vector> vl = getVertices();
            int vn = vl.size();
            std::vector<Vector> nl = getNormals();
            int nn = nl.size();
            if(v1 >= vn || v1 < 0) {
                printf("Error: Specified vertex (%d) in triangle (%d) does not exist\n", v1, (int)getTriangles().size());
                return -1;
            }
            if(v2 >= vn || v2 < 0) {
                printf("Error: Specified vertex (%d) in triangle (%d) does not exist\n", v2, (int)getTriangles().size());
                return -1;
            }
            if(v3 >= vn || v3 < 0) {
                printf("Error: Specified vertex (%d) in triangle (%d) does not exist\n", v3, (int)getTriangles().size());
                return -1;
            }
            if(n1 >= nn || n1 < 0) {
                printf("Error: Specified normal (%d) in triangle (%d) does not exist\n", n1, (int)getTriangles().size());
                return -1;
            }
            if(n2 >= nn || n2 < 0) {
                printf("Error: Specified normal (%d) in triangle (%d) does not exist\n", n2, (int)getTriangles().size());
                return -1;
            }
            if(n3 >= nn || n3 < 0) {
                printf("Error: Specified normal (%d) in triangle (%d) does not exist\n", n3, (int)getTriangles().size());
                return -1;
            }
            addObject(new Triangle(vl[v1], vl[v2], vl[v3], nl[n1], nl[n2], nl[n3], curMat));
        }
        else if(str == "space_threshold") {
            int thresh;
            scn >> thresh;
            setSpatialThreshold(thresh);
        }
        else if(str == "space_depth") {
            int depth;
            scn >> depth;
            setSpatialDepth(depth);
        }
        else if(str == "sample_rate") {
            int rate;
            scn >> rate;
            setSampleRate(rate);
        }
    }
    //No spatial structure set, defaulting to none
    if(!spaceStruct) {
        spaceStruct = new Space();
    }
    clearPools();
    scn.close();
    return 1;
}

std::vector<Sphere> Scene::getSpheres() const {
	std::vector<Sphere> list;
	Object* o;
	Sphere* s;

	for(int i = 0; i < (signed)objs.size(); i++) {
		o = objs[i];
		s = dynamic_cast<Sphere*>(o);
		if(s) {
			list.push_back(*s);
		}
	}

	o = 0;
	s = 0;

	return list;
}

std::vector<Triangle> Scene::getTriangles() const {
	std::vector<Triangle> list;

	for(int i = 0; i < (signed)objs.size(); i++) {
		Object* o = objs[i];
		Triangle* tPtr = dynamic_cast<Triangle*>(o);
		if(tPtr) {
			list.push_back(*tPtr);
		}
	}

	return list;
}

//Returns the distance to the viewing plane based on the angle from the
//viewing point and the height of the plane
float Scene::getPlaneDist() const {
    return img.getHeight() / (2.0 * tan(camera.getHalfFOV()));
}

void Scene::clearPools() {
    vertexPool.clear();
    normalPool.clear();
}

/********************
 * Vector
 ********************/
Vector::Vector() {
    x = y = z = 0;
}

Vector::Vector(float val) {
    x = y = z = val;
}

Vector::Vector(float xval, float yval, float zval) {
    x = xval;
    y = yval;
    z = zval;
}

float Vector::dot(const Vector& u, const Vector& v) {
    return (u.x * v.x) + (u.y * v.y) + (u.z * v.z);
}

Vector Vector::cross(const Vector& u, const Vector& v) {
    return Vector(u.y*v.z-u.z*v.y, u.z*v.x-u.x*v.z, u.x*v.y-u.y*v.x);
}

float Vector::length(const Vector& u, const Vector& v) {
    Vector dist = u - v;
    return sqrt((dist.x * dist.x) + (dist.y * dist.y) + (dist.z * dist.z));
}

float Vector::lengthSq(const Vector& u, const Vector& v) {
    const Vector dist = u - v;
    return (dist.x * dist.x) + (dist.y * dist.y) + (dist.z * dist.z);
}

float Vector::magnitude() {
    return sqrt((x * x) + (y * y) + (z * z));
}

float Vector::magnitudeSq() {
    return (x * x) + (y * y) + (z * z);
}

Vector Vector::norm() {
    float mag = magnitude();
    return Vector(x/mag, y/mag, z/mag);
}

Vector Vector::operator*(const float& c) const {
    return Vector(x*c, y*c, z*c);
}

Vector& Vector::operator*=(const Vector& u) {
    this->x *= u.x;
    this->y *= u.y;
    this->z *= u.z;
    return *this;
}

Vector Vector::operator+(const Vector& u) const {
    return Vector(x+u.x, y+u.y, z+u.z);
}

Vector Vector::operator+(const float& c) const {
    return Vector(x+c, y+c, z+c);
}

Vector& Vector::operator+=(const Vector& u) {
    this->x += u.x;
    this->y += u.y;
    this->z += u.z;
    return *this;
}

Vector& Vector::operator+=(const float& c) {
    this->x += c;
    this->y += c;
    this->z += c;
    return *this;
}

Vector Vector::operator-(const Vector& u) const {
    return Vector(x-u.x, y-u.y, z-u.z);
}

Vector Vector::operator-(const float& c) const {
    return Vector(x-c, y-c, z-c);
}

Vector& Vector::operator-=(const Vector& u) {
    this->x -= u.x;
    this->y -= u.y;
    this->z -= u.z;
    return *this;
}

Vector& Vector::operator-=(const float& c) {
    this->x -= c;
    this->y -= c;
    this->z -= c;
    return *this;
}

Vector Vector::operator/(const float& c) const {
    return Vector(x/c, y/c, z/c);
}

Vector& Vector::operator/=(const float& c) {
    this->x /= c;
    this->y /= c;
    this->z /= c;
    return *this;
}

/********************
 * Material
 ********************/
Material::Material() {
    ambientColor = Vector();
    diffuseColor = Vector(1);
    specularColor = Vector();
    cosPow = 5;
    transmissiveColor = Vector();
    ior = 1;
}
 
Material::Material(const Vector& ambient, const Vector& diffuse, const Vector& specular) {
    ambientColor = ambient;
    diffuseColor = diffuse;
    specularColor = specular;
    cosPow = 5;
    transmissiveColor = Vector();
    ior = 1;
}

Material::Material(const Vector& amb, const Vector& dif, const Vector& spec, const Vector& trans, float pow, float iref) {
    ambientColor = amb;
    diffuseColor = dif;
    specularColor = spec;
    cosPow = pow;
    transmissiveColor = trans;
    ior = iref;
}

/********************
 * Ray
 ********************/
Ray::Ray() {
    p = Vector(); //< init to (0, 0, 0)
    d = Vector();
}

Ray::Ray(const Vector& pos, const Vector& dir) {
    p = pos;
    d = dir;
}

/********************
 * Intersection
 ********************/
Intersect::Intersect() {
    normal = Vector();
    r = Ray();
    dist = 0;
    p = Vector();
    m = Material();
}

Intersect::Intersect(const Vector& norm, const Ray& ray, double distsq, const Vector& point, const Material& mat) {
    normal = norm;
    r = ray;
    dist = distsq;
    p = point;
    m = mat;
}

/********************
 * Object Base
 ********************/
Object::Object() {
    mat = Material();
    id = BASE;
}

Object::Object(enum ObjectType objID) {
    mat = Material();
    id = objID;
}

Object::Object(const Material& material) {
    mat = material;
    id = BASE;
}

Object::Object(const Material& material, enum ObjectType objID) {
    mat = material;
    id = objID;
}

Intersect* Object::intersect(const Ray& trace, float dmin, float dmax) const {
	return new Intersect();
}

/********************
 * Sphere
 ********************/
Sphere::Sphere() : Object(CIRCLE) {
    position = Vector();
    radius = 1.0f;
}

Sphere::Sphere(const Vector& pos, float rad, const Material& material) : Object(material, CIRCLE) {
    position = pos;
    radius = rad;
}

Intersect* Sphere::intersect(const Ray& trace, float dmin, float dmax) const {
	Intersect* in = 0;

    Vector posit = trace.getPosition();
    Vector direc = trace.getDirection();
	double dd = Vector::dot(direc, direc);

	Vector ec = posit - getPosition();
	double dec = Vector::dot(direc, ec);
	
	double det = (dec*dec) - dd*(Vector::dot(ec, ec) - (getRadius()*getRadius()));
	if (det < 0.0) {
		return 0;
	}
	
	double sqrtdet = sqrt(det);
	double t1 = (-dec + sqrtdet) / dd;
	double t2 = (-dec - sqrtdet) / dd;
	//t is the number of rays it takes to get to the object. Make that into
	//a number to check bounds with (distance to intersect)
	Vector p1 = direc * t1;
	Vector p2 = direc * t2;
	double d1 = p1.magnitudeSq();
	double d2 = p2.magnitudeSq();
	if(t1 < t2) {
		if(t1 > 0 && d1 > dmin && d1 < dmax) {
			Vector point = posit + p1;
			if(in == 0) {
				in = new Intersect((point - getPosition()).norm(), trace, d1, point, getMaterial());
			}
			else if(d1 < in->dist) {
				in->r = trace;
				in->dist = d1;
				in->m = getMaterial();
				in->p = point;
				in->normal = (in->p - getPosition()).norm();
			}
		}
		else if(t2 > 0 && d2 > dmin && d2 < dmax) {
			Vector point = posit + p2;
			if(in == 0) {
				in = new Intersect((point - getPosition()).norm(), trace, d2, point, getMaterial());
			}
			else if(d2 < in->dist) {
				in->r = trace;
				in->dist = d2;
				in->m = getMaterial();
				in->p = point;
				in->normal = (in->p - getPosition()).norm();
			}
		}
	}
	else {
		if(t2 > 0 && d2 > dmin && d2 < dmax) {
			Vector point = posit + p2;
			if(in == 0) {
				in = new Intersect((point - getPosition()).norm(), trace, d2, point, getMaterial());
			}
			else if(d2 < in->dist) {
				in->r = trace;
				in->dist = d2;
				in->m = getMaterial();
				in->p = point;
				in->normal = (in->p - getPosition()).norm();
			}
		}
		else if(t1 > 0 && d1 > dmin && d1 < dmax) {
			Vector point = posit + p1;
			if(in == 0) {
				in = new Intersect((point - getPosition()).norm(), trace, d1, point, getMaterial());
			}
			else if(d1 < in->dist) {
				in->r = trace;
				in->dist = d1;
				in->m = getMaterial();
				in->p = point;
				in->normal = (point - getPosition()).norm();
			}
		}
	}
    return in;
}

/********************
 * Triangle
 ********************/
Triangle::Triangle() : Object(TRIANGLE) {
    vertices[0] = vertices[1] = vertices[2] = Vector();
    normals[0] = normals[1] = normals[2] = Vector(0, 1, 0);
    ntri = false;
}

Triangle::Triangle(const Vector& v1, const Vector& v2, const Vector& v3) : Object(TRIANGLE) {
    vertices[0] = v1;
    vertices[1] = v2;
    vertices[2] = v3;
    normals[0] = normals[1] = normals[2] = Vector::cross(v2-v1, v3-v1).norm();
    ntri = false;
}

Triangle::Triangle(const Vector& v1, const Vector& v2, const Vector& v3, const Material& m) : Object(m, TRIANGLE) {
    vertices[0] = v1;
    vertices[1] = v2;
    vertices[2] = v3;
    normals[0] = normals[1] = normals[2] = Vector::cross(v2-v1, v3-v1).norm();
    ntri = false;
}

Triangle::Triangle(const Vector& v1, const Vector& v2, const Vector& v3, const Vector& n1, const Vector& n2, const Vector& n3) : Object(TRIANGLE) {
    vertices[0] = v1;
    vertices[1] = v2;
    vertices[2] = v3;
    normals[0] = n1;
    normals[1] = n2;
    normals[2] = n3;
    ntri = true;
}

Triangle::Triangle(const Vector& v1, const Vector& v2, const Vector& v3, const Vector& n1, const Vector& n2, const Vector& n3, const Material& m) : Object(m, TRIANGLE) {
    vertices[0] = v1;
    vertices[1] = v2;
    vertices[2] = v3;
    normals[0] = n1;
    normals[1] = n2;
    normals[2] = n3;
    ntri = true;
}

Vector& Triangle::operator[](const int index) {
    if(index < 0 || index > 2) {
        throw std::out_of_range("Triangle index out of range");
    }
    return vertices[index];
}

Intersect* Triangle::intersect(const Ray& trace, float dmin, float dmax) const {
	Intersect* in = 0;

    Vector rd = trace.getDirection();
    Vector rp = trace.getPosition();

	Vector ae = getVertex(0) - rp;
	Vector ab = getVertex(0) - getVertex(1);
	Vector ac = getVertex(0) - getVertex(2);
	double detA = ab.x*(ac.y*rd.z-rd.y*ac.z) + ab.y*(ac.z*rd.x-rd.z*ac.x) + ab.z*(ac.x*rd.y-ac.y*rd.x);
	double dett = ac.z*(ab.x*ae.y-ab.y*ae.x) + ac.y*(ae.x*ab.z-ab.x*ae.z) + ac.x*(ab.y*ae.z-ab.z*ae.y);
	double tval = dett / detA;
	if(tval > 0) {
		return 0;
	}
	double detb = ae.x*(ac.y*rd.z-rd.y*ac.z) + ae.y*(ac.z*rd.x-rd.z*ac.x) + ae.z*(ac.x*rd.y-ac.y*rd.x);
	double detg = rd.z*(ab.x*ae.y-ae.x*ab.y) + rd.y*(ab.z*ae.x-ae.z*ab.x) + rd.x*(ab.y*ae.z-ae.y*ab.z);
	double beta = detb / detA;
	double gamma = detg / detA;
	if(beta >= 0 && beta <= 1 && gamma >= 0  && gamma <= 1 && beta + gamma <= 1) {
        Vector point = getVertex(0) + 
                (((getVertex(1) - getVertex(0)) * beta) + ((getVertex(2) - getVertex(0)) * gamma));
		double dist = Vector::lengthSq(point, rp);
		if(dist < dmin || dist > dmax) {
			return 0;
		}
		if(in == 0) {
			Vector normal;
			if(isNormal()) {
				//Linearly Interpolate
				//multiply each vertex norm by the corresponding barycentric coordinant
				//add and renormalize
				double alpha = 1.0 - (beta + gamma);
				double ang1 = Vector::dot(getNormal(0), rd);
				double ang2 = Vector::dot(getNormal(1), rd);
				double ang3 = Vector::dot(getNormal(2), rd);
				Vector norm1 = (ang1 > 0) ? getNormal(0)*-1.0f : getNormal(0);
				Vector norm2 = (ang2 > 0) ? getNormal(1)*-1.0f : getNormal(1);
				Vector norm3 = (ang3 > 0) ? getNormal(2)*-1.0f : getNormal(2);
                normal = ((norm1 * beta) + (norm2 * gamma) + (norm3 * alpha)).norm();
			}
			else {
				double ang = Vector::dot(getNormal(0), rd);
				if(ang > 0) {
					normal = getNormal(0) * -1.0f;
				}
				else {
					normal = getNormal(0);
				}
			}
			in = new Intersect(normal, trace, dist, point, getMaterial());
		}
		else if(dist < in->dist) {
			Vector normal;
			if(isNormal()) {
				//Linearly Interpolate
				double alpha = 1.0 - (beta + gamma);
				double ang1 = Vector::dot(getNormal(0), rd);
				double ang2 = Vector::dot(getNormal(1), rd);
				double ang3 = Vector::dot(getNormal(2), rd);
				Vector norm1 = (ang1 > 0) ? getNormal(0)*-1.0f : getNormal(0);
				Vector norm2 = (ang2 > 0) ? getNormal(1)*-1.0f : getNormal(1);
				Vector norm3 = (ang3 > 0) ? getNormal(2)*-1.0f : getNormal(2);
                normal = ((norm1 * beta) + (norm2 * gamma) + (norm3 * alpha)).norm();
			}
			else {
				double ang = Vector::dot(getNormal(0), rd);
				if(ang > 0) {
					normal = getNormal(0) * -1.0f;
				}
				else {
					normal = getNormal(0);
				}
			}
			in->r = trace;
			in->dist = dist;
			in->p = point;
			in->m = getMaterial();
			in->normal = normal;
		}
	}
    return in;
}

/********************
 * Light
 ********************/
Light::Light() {
    color = Vector();
    position = Vector();
    direction = Vector(0, 1, 0);
    angle1 = M_PI_4;
    angle2 = M_PI_2;
    ltype = AMBIENT;
}

Light::Light(const Vector& lightColor) {
    color = lightColor;
    position = Vector();
    direction = Vector(0, 1, 0);
    angle1 = M_PI_4;
    angle2 = M_PI_2;
    ltype = AMBIENT;
}

Light::Light(const Vector& lightColor, const Vector& lightPosDir, enum LightType type) {
    color = lightColor;
    if(type == POINT) {
        position = lightPosDir;
        direction = Vector(0, 1, 0);
    }
    else if(type == DIRECTIONAL) {
        position = Vector();
        direction = lightPosDir;
    }
    ltype = type;
    angle1 = M_PI_4;
    angle2 = M_PI_2;
}

Light::Light(const Vector& lightColor, const Vector& lightPos, const Vector& lightDir, float spotAngle, float maxAngle) {
    color = lightColor;
    position = lightPos;
    direction = lightDir;
    angle1 = spotAngle;
    angle2 = maxAngle;
    ltype = SPOT;
}

/********************
 * Camera
 ********************/
Camera::Camera() {
    position = Vector();
    direction = Vector(1, 0, 0);
    up = Vector(0, 1, 0);
    halfAngle = 45.0f;
}

Camera::Camera(const Vector& pos, const Vector& dir, const Vector& upDir, float fov) {
    position = pos;
    direction = dir;
    up = upDir;
    halfAngle = fov;
}

/********************
 * Image
 ********************/
rt::Image::Image() {
    filename = "raytraced.bmp";
    width =  640;
    height = 480;
}

rt::Image::Image(const std::string& name) {
    filename = name;
    width = 640;
    height = 480;
}

rt::Image::Image(const std::string& name, int imageWidth, int imageHeight) {
    filename = name;
    width = imageWidth;
    height = imageHeight;
}

/********************
 * Plane
 ********************/

Plane::Plane() : Object() {
    normal = Vector();
    point = Vector();
}

/********************
 * Printing
 ********************/
void printScene(Scene *scn) {
    printf("\nCamera:\n");
    printCamera(scn->getCamera());
    printf("\n");
    
    printf("Spheres:\n");
    int num = scn->getSpheres().size();
    if(num != 0) {
        for (int i = 0; i < num; i++) {
            printSphere(scn->getSpheres()[i]);
        }
    }
    else {
        printf("No Spheres\n");
    }
    printf("\n");
    
    printf("Image:\n");
    printImage(scn->getImage());
    printf("\n");
    
    printf("Background Color: ");
    printVector(scn->getBGColor());
    printf("\n");
    
    printf("Lights:\n");
    num = scn->getLights().size();
    if (num > 0) {
        for (int i = 0; i < num; i++) {
            printLight(scn->getLights()[i]);
        }
    }
    else {
        printf("No Lights\n");
    }
    printf("\n");
    
    printf("Ambient Light:\n");
    printLight(scn->getAmbLight());
    printf("\n");
    
    printf("Depth: %d\n", scn->getDepth());
    
    if(scn->getProjType() == PERSP) {
        printf("Projection Type: Perspective\n\n");
    }
    else {
        printf("Projection Type: Orthographic\n\n");
    }
    
    printf("Triangles:\n");
    num = scn->getTriangles().size();
    if(num != 0) {
		for(int i = 0; i < num; i++) {
			printTriangle(scn->getTriangles()[i]);
		}	
	}
	else {
		printf("No Triangles\n");
	}
	printf("\n");
	/*
	printf("Planes:\n");
	num = scn->planes.size();
	if(num != 0) {
		for(int i = 0; i < num; i++) {
			printPlane(scn->planes[i]);
		}
	}
	else {
		printf("No Planes\n");
	}
    printf("\n");
    */
}

void printCamera(Camera camera) {
    printf("Position: ");
    printVector(camera.getPosition());
    printf("Direction: ");
    printVector(camera.getDirection());
    printf("Up Vector: ");
    printVector(camera.getUpDirection());
    printf("Half Angle: %f\n", camera.getHalfFOV());
}

void printTriangle(Triangle tri) {
    printf("Vertex1: ");
    printVector(tri.getVertex(0));
    printf("Vertex2: ");
    printVector(tri.getVertex(1));
    printf("Vertex3: ");
    printVector(tri.getVertex(2));
    printf("Normal: ");
    printVector(tri.getNormal(0));
    if(tri.isNormal()) {
        printf("Normal2: ");
        printVector(tri.getNormal(1));
        printf("Normal3: ");
        printVector(tri.getNormal(2));
    }
    printf("Material:\n");
    printMaterial(tri.getMaterial());
}

void printSphere(Sphere sph) {
    printf("Position: ");
    printVector(sph.getPosition());
    printf("Radius: %f\n", sph.getRadius());
    printf("Material:\n");
    printMaterial(sph.getMaterial());
}

void printMaterial(Material mat) {
    printf("Ambient Color: ");
    printVector(mat.getAmbient());
    printf("Diffuse Color: ");
    printVector(mat.getDiffuse());
    printf("Specular Color: ");
    printVector(mat.getSpecular());
    printf("Cosin Power: %f\n", mat.getCosPower());
    printf("Transmissive Color: ");
    printVector(mat.getTransmissive());
    printf("ior: %f\n", mat.getIndexRefract());
}

void printLight(Light l) {
    printf("Type: ");
    switch(l.getType()) {
        case DIRECTIONAL:
            printf("Directional\n");
            printf("Direction: ");
            printVector(l.getDirection());
            break;
        case POINT:
            printf("Point\n");
            printf("Position: ");
            printVector(l.getPosition());
            break;
        case SPOT:
            printf("Spot\n");
            printf("Position: ");
            printVector(l.getPosition());
            printf("Direction: ");
            printVector(l.getDirection());
            printf("Angle: (%f %f)\n", l.getSpotAngle(), l.getMaxAngle());
            break;
        default:
            printf("Ambient\n");
    }
    printf("Color: ");
    printVector(l.getColor());
}

void printImage(rt::Image img) {
    printf("File Name: %s\n", img.getFileName().c_str());
    printf("Width: %d\n", img.getWidth());
    printf("Height: %d\n", img.getHeight());
}

void printVector(Vector fVec) {
    printf("(%f %f %f)\n", fVec.x, fVec.y, fVec.z);
}

#include "../include/raytrace.hpp"
#include <cmath>
#include <limits>
#include <cstdlib>
#include <ctime>

RayTrace::RayTrace() {
    std::srand(std::time(0));
}

//Performs a raytrace on the Scene scn with projection type proj
Image* RayTrace::rayTrace(Scene& scn) {
	#ifdef DEBUG
    printf("Beginning Trace\n");
    printf("Creating Spacial Structure\n");
    #endif
    
    spaceStruct = scn.getSpatialStruct();
    spaceStruct->make(scn, scn.getSpatialDepth());
    spaceStruct->print();

	ProjType proj = scn.getProjType();
    int width = scn.getImage().getWidth(), height = scn.getImage().getHeight();
    Image* dest = new Image(width, height);
    Camera c = scn.getCamera();
    Vector p1, p2;
    //Distance to the viewing plane
    double planeDist = scn.getPlaneDist();
    getExtremePoints(c, planeDist, width, height, p1, p2);
    #ifdef DEBUG
    printf("P1: (%f %f %f)\n", p1.x, p1.y, p1.z);
    printf("P2: (%f %f %f)\n", p2.x, p2.y, p2.z);
    printf("Plane Dist: %f\n", planeDist);
    #endif
    int totalpix = width * height;
    int tenper = .1 * totalpix;
    int count = 0;
    int sampleNm = scn.getSampleRate();
    bool sample = false;
    if(sampleNm > 1) {
		sample = true;
	}
    Pixel pix;
    #ifdef PARALLEL
    #pragma omp parallel for collapse(2) schedule(static, 125)
    #endif
    for(int x = 0; x < width; x++) {
        for(int y = 0; y < height; y++) {
			if((y + (x * height)) % tenper == 0) {
				printf("%d%%\n", count);
				#ifdef PARALLEL
				#pragma omp atomic
				#endif
				count += 10;
			}
            //Get ray from the viewpoint to (x, y) point
            if(sample) {
                Vector* color = new Vector[sampleNm];
				for(int j = 0; j < sampleNm; j++) {
					Ray trace = getRay(x, y, width, height, p1, p2, planeDist, c, proj, sample);
					color[j] = evaluateRayTree(scn, trace, 0);
				}
				Vector col = ave(color, sampleNm);
				pix.SetClamp(col.x*255.0, col.y*255.0, col.z*255.0);
				dest->GetPixel(x, y) = pix;
                delete[] color;
			}
            else {
				Ray trace = getRay(x, y, width, height, p1, p2, planeDist, c, proj, sample);
				Vector col = evaluateRayTree(scn, trace, 0);
				pix.SetClamp(col.x*255.0, col.y*255.0, col.z*255.0);
				dest->GetPixel(x, y) = pix;
			}
        }
        //printf("Row %d\n", x);
    }
    printf("100%%\n");
    deleteSpatialStructure();
    return dest;
}

Vector RayTrace::evaluateRayTree(const Scene& scn, const Ray& ray, int depth) const {
    if(depth > scn.getDepth()) {
        return scn.getBGColor();
        //return Vector();
    }
    Intersect* in;
    bool hit = false;
    //Go through each object and check for intersection
    if((in = intersect(ray, scn, .001, std::numeric_limits<double>::infinity()))) {
        hit = true;
    }
    if(hit) {
        Vector pixcol = getColor(in, scn, depth);
        delete in;
        in = 0;
        return pixcol;
    }
    else {
        return scn.getBGColor();
    }
}

//Tests if there is an intersection between a ray and objects
Intersect* RayTrace::intersect(const Ray& trace, const Scene& scn, double dmin, double dmax) const {
	Intersect* min = 0;		
	std::vector<Object*> list = spaceStruct->intersect(trace, dmin, dmax);
    
    Intersect* tmp = 0;
    //printf("Intersect Regular\n");
    for(int i = 0; i < (signed)list.size(); i++) {
    	tmp = list[i]->intersect(trace, dmin, dmax);
    	//Not Null (ray intersected)
    	if(tmp) {
    		if(!min) { //First intersection (min is still null)
    			min = new Intersect(tmp->normal, tmp->r, tmp->dist, tmp->p, tmp->m);
    			delete tmp;
    			tmp = 0;
    		}
    		else {
    			min->normal = tmp->normal;
				min->r = tmp->r;
				min->dist = tmp->dist;
				min->p = tmp->p;
				min->m = tmp->m;
				delete tmp;
				tmp = 0;
    		}
    	}
    }
	return min;
}

//Returns a ray from the viewpoint to the pixel in position (x, y)
Ray RayTrace::getRay(int x, int y, int w, int h, const Vector& p1, const Vector& p2, double pd, const Camera& c, ProjType proj, bool sample) const {
    Ray e;
    Vector pos = c.getPosition();
    Vector right = (Vector::cross(c.getDirection(), c.getUpDirection())).norm();
    Vector down = (c.getUpDirection() * -1.0f).norm();
    double r1, r2;
    if(sample) {
		r1 = (double)std::rand() / RAND_MAX;
		r2 = (double)std::rand() / RAND_MAX;
	}
	else {
		r1 = r2 = .5;
	}
    if (proj == PERSP) {
        Vector dir;
        //Vector from p1 to p2
        Vector ptop = p2 - p1;
        //Project that vector onto the right vector to get upper right point
        Vector ur = right * Vector::dot(ptop, right);
        ur = ur * ((double)x/w + (r1/w));
        //Project ptop onto down vector to get bottom left point
        Vector bl = down * Vector::dot(ptop, down);
        bl = bl * ((double)y/h + (r2/h));
        //Add x percent of first projection and y percent of second projection
        dir = ur + bl;
        dir = dir + p1;
        e = Ray(pos, dir.norm());
    }
    else {
        Vector p;
        //Vector from p1 to p2
        Vector ptop = p2 - p1;
        //Project that vector onto the right vector to get upper right point
        Vector ur = right * Vector::dot(ptop, right);
        ur = ur * ((double)x/w + (r1/w));
        //Project ptop onto down vector to get bottom left point
        Vector bl = down * Vector::dot(ptop, down);
        bl = bl * ((double)y/h + (r2/h));
        //Add x percent of first projection and y percent of second projection
        p = ur + bl;
        p = p + p1;
        e = Ray(p, c.getDirection());
    }
    return e;
}

//Returns a pixel with the background color
Vector RayTrace::getColor(const Intersect* i, const Scene& scn, int depth) const {
	std::vector<Light> lights = scn.getLights();
	Light a = scn.getAmbLight();
	int nlights = lights.size();
	Material m = i->m;
	Vector dif = m.getDiffuse();
    Vector spec = m.getSpecular();
	float rval, gval, bval;
	//Get the base color(La)
	Vector aColor = m.getAmbient();
	rval = aColor.x;
	gval = aColor.y;
	bval = aColor.z;
	//Figure out diffuse light using Lambertian shading (Ld)
	//Point that the ray intersected the sphere
	Vector point = i->p;
	Vector direct = i->r.getDirection();
	//Normal norm(P-C)
	Vector normal = i->normal;
	//Multiply material ambient color by the light ambient color
	rval *= a.getColor().x;
	gval *= a.getColor().y;
	bval *= a.getColor().z;
	//Loop through each lightsource
	Ray shadow;
    shadow.setPosition(point);
	Vector I;
	Light L;
    Intersect* in = 0;
	for(int j = 0; j < nlights; j++) {
		L = lights[j];
		//Lambert
		//If the ray from the point in the direction of the light intersects
		//anything, skip the rest
        double dist = 0.0f;
        if(L.getType() == DIRECTIONAL) {
            I = L.getDirection() * -1.0;
            I = I.norm();
            shadow.setDirection(I);
            dist = std::numeric_limits<double>::infinity();
        } 
        else { //Point or Spot light
            I = L.getPosition() - point;
            dist = I.magnitudeSq();
            I = I.norm();
            shadow.setDirection(I);
        }
		//Use really small number in intersect so that it doesn't intersect itself
		if((in = intersect(shadow, scn, .001, dist))) {
            delete in;
			continue;
		}
        /***
         * TODO - Update rest of the Lighting code to use the one loop
         ***/
		double dotni = Vector::dot(normal, I);
		double cosAlpha = (dotni > 0) ? dotni : 0;
		Vector illum = L.getColor();
        if(L.getType() == POINT) {
            illum = illum * (1.0/dist);
        }
        else if(L.getType() == SPOT) {
            double alpha = acos(cosAlpha);
            //Acts like a point light
            if(alpha < L.getSpotAngle()) {
                illum = L.getColor() * (1.0/dist);
            }
            //Greater than angle2, light contributes nothing
            else if(alpha > L.getMaxAngle()) {
                illum = Vector();
            }
            //Linearly interpolate
            else {
                //Get amount alpha is between the 2 angles (angle1=1, angle2=0)
                double amt = 1 - ((alpha - L.getSpotAngle()) / (L.getMaxAngle() - L.getSpotAngle()));
                //Multiply light by amount
                illum = L.getColor() * ((1.0/dist)*amt);
            }
        }
		rval += dif.x * illum.x * cosAlpha;
		gval += dif.y * illum.y * cosAlpha;
		bval += dif.z * illum.z * cosAlpha;
		//Phong
		Vector ref = (normal * (2.0*dotni)) - I;
		Vector V = direct * -1.0f;
        double powval = Vector::dot(V.norm(), ref.norm());
        double pspec;
        if(powval < 0.0) {
            pspec = 0.0;
        }
        else {
            pspec = pow(powval, m.getCosPower());
        }
		rval += spec.x * pspec * illum.x;
		gval += spec.y * pspec * illum.y;
		bval += spec.z * pspec * illum.z;
	}
    Vector refColor;
    Vector rdir = direct;
	Vector irdir = rdir * -1.0f;
    ///Reflect Ray
    if(spec.x != 0 && spec.y != 0 && spec.z != 0) {
		Ray reflection;
		reflection.setPosition(point);
		reflection.setDirection((normal * (2.0f*Vector::dot(normal, irdir))) - irdir);
		refColor = evaluateRayTree(scn, reflection, depth+1);
		rval += spec.x * refColor.x;
		gval += spec.y * refColor.y;
		bval += spec.z * refColor.z;
	}
    ///Refract
    Vector trans = m.getTransmissive();
    if(trans.x != 0 && trans.y != 0 && trans.z != 0) {
		Ray refraction;
		refraction.setPosition(point);
		double ior;
		double dni = Vector::dot(irdir, normal);
		if(dni <= 0) { //going into object
			ior = m.getIndexRefract();
		}
		else {
			ior = 1.0 / m.getIndexRefract();
		}
		// (nr*dot(N,I)-sqrt(1-nr^2(1-dot(N,I)^2)))*N - nr*I
		// nr*I + (nr*dot(I,N)-sqrt(1-(nr*nr)*(1-dot(I,N)^2)))*N
		//Total internal refraction if sqrt < 0
		double tir = 1.0 - (ior*ior) * (1.0 - (dni*dni));
		if(tir >= 0) {
			Vector refdir;
			if(dni >= 0) {
                refdir = (normal * (ior*dni-sqrt(tir))) - (irdir * ior);
			}
			else {
                refdir = (normal * (ior*dni+sqrt(tir))) - (irdir * ior);
			}
			refraction.setDirection(refdir.norm());
			refColor = evaluateRayTree(scn, refraction, depth+1);
			rval += trans.x * refColor.x;
			gval += trans.y * refColor.y;
			bval += trans.z * refColor.z;
		}
	}
    
    return Vector(rval, gval, bval);
}

//Gets the extreme points of top left and bottom right
void RayTrace::getExtremePoints(const Camera& c, double d, double w, double h, Vector& p1, Vector& p2) const {
	//p1 is the top left, p2 is the bottom right
	Vector p0 = c.getPosition();
	Vector up = c.getUpDirection();
	Vector right = Vector::cross(c.getDirection(), up);
	//Vector from the viewpoint to the center
	p0 = p0 + (c.getDirection() * d);
	//Add on the offset for the 2 extreme points (-x, -y) and (+x, +y)
	p1 = p0 + (right * (w/2.0)); //< Add x part
	p2 = p0 - (right * (w/2.0));
	p1 = p1 + (up * (h/2.0)); //< Add y part
	p2 = p2 - (up * (h/2.0));
}

void RayTrace::deleteSpatialStructure() {
    if(!spaceStruct) {
    	delete spaceStruct;
    } 
    spaceStruct = 0;
}

/**
* Vector Operations
*/

Vector RayTrace::ave(Vector v[], int num) {
	if(num == 1) {
		return v[0];
	}
	Vector u = v[0];
	for(int i = 1; i < num; i++) {
		u = u + v[i];
	}
	u = u / num;
	return u;
}


/**********
 * Space
 **********/

Space::Space() {
	id = 0;
}

Space::Space(int structureID) {
	id = structureID;
}

Space::~Space() {
	objects.clear();
}

void Space::make(Scene& scn, int depth) {
	objects = scn.getObjects();
}

void Space::print() const {
	printf("Number of Objects: %d\n", (signed)objects.size());
}

std::vector<Object*> Space::intersect(const Ray& trace, double dmin, double dmax) const {
	return objects;
}

#ifndef PARSE_H_
#define PARSE_H_

#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <string>
#include <vector>
#include <array>

//Object ID's (in order of when they were added)
//These are here for the purpose of identification for the spatial structure
//    0-Base
//    1-Circle
//    2-Triangle
enum ObjectType {
    BASE,
    CIRCLE,
    TRIANGLE
};

enum ProjType {
    ORTHO,
    PERSP
};

enum LightType {
    AMBIENT,
    DIRECTIONAL,
    POINT, 
    SPOT
};

class Vector {
    public:
        Vector();
        Vector(float val);
        Vector(float xval, float yval, float zval);
        
        float x;
        float y;
        float z;
        
        //Performs a dot product operation on the 2 vectors
        static float dot(const Vector& u, const Vector& v);
        //Performs a cross prooduct
        static Vector cross(const Vector& u, const Vector& v);
        //Finds the length of the distance between the 2 vectors (same as magnitude(u-v))
        static float length(const Vector& u, const Vector& v);
        //Square version of length()
        static float lengthSq(const Vector& u, const Vector& v);
        static Vector zero() { return Vector(); }
        static Vector one() { return Vector(1); }
        static Vector up() { return Vector(0, 1, 0); }
        static Vector down() { return Vector(0, -1, 0); }
        static Vector left() { return Vector(-1, 0, 0); }
        static Vector right() { return Vector(1, 0, 0); }
        static Vector forward() { return Vector(0, 0, 1); }
        static Vector backward() { return Vector(0, 0, -1); }
        
        //Returns the magnitude of the vector
        float magnitude();
        //Finds the magnitude but does not take the square root
        float magnitudeSq();
        //Normalizes the vector
        Vector norm();
        
        //Multiply vector
        Vector operator*(const float& c) const;
        Vector& operator*=(const Vector& u);
        //Adds 2 vectors
        Vector operator+(const Vector& u) const;
        Vector operator+(const float& c) const;
        Vector& operator+=(const Vector& u);
        Vector& operator+=(const float& c);
        //Subtracts 2 vectors
        Vector operator-(const Vector& u) const;
        Vector operator-(const float& c) const;
        Vector& operator-=(const Vector& u);
        Vector& operator-=(const float& c);
        //Divides each entry in the vector by a constant
        Vector operator/(const float& c) const;
        Vector& operator/=(const float& c);
};

class Material {
    public:
        Material();
        Material(const Vector& ambient, const Vector& diffuse, const Vector& specular);
        Material(const Vector& amb, const Vector& dif, const Vector& spec, const Vector& trans, float pow, float iref);
        
        void setAmbient(const Vector& amb) { ambientColor = amb; }
        void setDiffuse(const Vector& dif) { diffuseColor = dif; }
        void setSpecular(const Vector& spec) { specularColor = spec; }
        void setTransmissive(const Vector& trns) { transmissiveColor = trns; }
        void setCosPower(float pow) { cosPow = pow; }
        void setIndexRefract(float index) { ior = index; }
        
        Vector getAmbient() const { return ambientColor; }
        Vector getDiffuse() const { return diffuseColor; }
        Vector getSpecular() const { return specularColor; }
        Vector getTransmissive() const { return transmissiveColor; }
        float getCosPower() const { return cosPow; }
        float getIndexRefract() const { return ior; }
    private:
        Vector ambientColor;
        Vector diffuseColor;
        Vector specularColor;
        
        float cosPow; //Phong cosine power for specular highlights
        Vector transmissiveColor;
        float ior;
};

class Ray {
    public:
        Ray();
        Ray(const Vector& pos, const Vector& dir);
        
        void setPosition(const Vector& pos) { p = pos; }
        void setDirection(const Vector& dir) { d = dir; }
        
        Vector getPosition() const { return p; }
        Vector getDirection() const { return d; }
    private:
        Vector p;
        Vector d;
};

class Intersect {
    public:
        Intersect();
        Intersect(const Vector& norm, const Ray& ray, double distsq, const Vector& point, const Material& mat);
        
        //Normal of intersect point
        Vector normal;
        //Intersected ray
        Ray r;
        //t value of intersection (Distancesq)
        double dist;
        //t replaced with point of intersection
        Vector p;
        //Intersection material
        Material m;
};

class Object {
    public:
        Object();
        Object(enum ObjectType objID);
        Object(const Material& material);
        Object(const Material& material, enum ObjectType objID);

        virtual Intersect* intersect(const Ray& trace, float dmin, float dmax) const;

        virtual void setMaterial(const Material& material) { mat = material; }

        virtual Material getMaterial() const { return mat; }

    protected:
        Material mat;
        enum ObjectType id;
};

class Sphere : public Object {
    public:
        Sphere();
        Sphere(const Vector& pos, float rad, const Material& material);
        
        Intersect* intersect(const Ray& trace, float dmin, float dmax) const;

        void setPosition(const Vector& pos) { position = pos; }
        void setRadius(const float& rad) { radius = rad; }
        void setMaterial(const Material& material) { mat = material; }
        
        Vector getPosition() const { return position; }
        float getRadius() const { return radius; }
        Material getMaterial() const { return mat; }
    private:
        Vector position;
        float radius;
};

class Triangle : public Object {
    public:
        Triangle();
        Triangle(const Vector& v1, const Vector& v2, const Vector& v3);
        Triangle(const Vector& v1, const Vector& v2, const Vector& v3, const Material& m);
        Triangle(const Vector& v1, const Vector& v2, const Vector& v3, const Vector& n1, const Vector& n2, const Vector& n3);
        Triangle(const Vector& v1, const Vector& v2, const Vector& v3, const Vector& n1, const Vector& n2, const Vector& n3, const Material& m);
        
        Intersect* intersect(const Ray& trace, float dmin, float dmax) const;

        void setMaterial(const Material& material) { mat = material; }
        void setVertex(const Vector& vert, int index) { vertices[clamp(index, 0, 2)] = vert; }
        void setVertices(const Vector verts[3]);
        void setNormal(const Vector& norm) { normals[0] = normals[1] = normals[2] = norm; ntri = false;}
        void setNormal(const Vector& norm, int index) { normals[clamp(index, 0, 2)] = norm; ntri = true; }
        void setNormals(const Vector norms[3]);
        
        Material getMaterial() const { return mat; }
        Vector getNormal() const { return normals[0]; }
        Vector getNormal(int index) const { return normals[clamp(index, 0, 2)]; }
        Vector getVertex(int index) const { return vertices[clamp(index, 0, 2)]; }
        std::vector<Vector> getVertices() const { return std::vector<Vector>(vertices, vertices+3); }
        std::vector<Vector> getNormals() const { return std::vector<Vector>(normals, normals+3); }
        bool isNormal() const { return ntri; } //< Returns wether the triangle has seperate normals for each point
        Vector& operator[](const int index);
    private:
        int clamp(int num, int min, int max) const { return (num < min) ? min : (num > max) ? max : num; }
        
        Vector vertices[3];
        Vector normals[3];
        bool ntri;
};

class Plane : public Object {
    public:
        Plane();
        Vector normal;
        Vector point;
    private:
        
};

class Light {
    public:
        Light();
        Light(const Vector& lightColor);
        Light(const Vector& lightColor, const Vector& lightPosDir, enum LightType type);
        Light(const Vector& lightColor, const Vector& lightPos, const Vector& lightDir, float spotAngle, float maxAngle);
        
        //these do not affect the type of light (i.e. changing direction of a spot
        //light has no effect)
        void setColor(const Vector& newColor) { color = newColor; }
        void setPosition(const Vector& newPos) { position = newPos; }
        void setDirection(const Vector& newDir) { direction = newDir; }
        void setSpotAngle(float angle) { angle1 = angle; }
        void setMaxAngle(float angle) { angle2 = angle; }
        void setType(enum LightType type) { ltype = type; }
        
        Vector getColor() const { return color; }
        Vector getPosition() const { return position; }
        Vector getDirection() const { return direction; }
        float getSpotAngle() const { return angle1; }
        float getMaxAngle() const { return angle2; }
        enum LightType getType() const { return ltype; }
        
    private:
        Vector color;         //Color of the light (All)
        Vector position;      //Position of the Light (Point, Spot)
        Vector direction;     //Direction the light is pointing (Directional, Spot)
        float angle1;         //Angle where it acts as a point light (Spot)
        float angle2;         //Max angle that the spot light reaches (Spot)
        enum LightType ltype; //Light type (0-ambient, 1-directional, 2-point, 3-spot)
};

class Camera {
    public:
        Camera();
        Camera(const Vector& pos, const Vector& dir, const Vector& upDir, float fov);
        
        void setPosition(const Vector& pos) { position = pos; }
        void setDirection(const Vector& dir) { direction = dir; }
        void setUpDirection(const Vector& upDir) { up = upDir; }
        void setFOV(float angle) { halfAngle = angle / 2.0f; }
        void setHalfFOV(float angle) { halfAngle = angle; }
        
        Vector getPosition() const { return position; }
        Vector getDirection() const { return direction; }
        Vector getUpDirection() const { return up; }
        float getFOV() const { return 2.0f * halfAngle; }
        float getHalfFOV() const { return halfAngle; }
    private:
        Vector position;
        Vector direction;
        Vector up;
        float halfAngle;
};

namespace rt { //Image conflicts with EasyBMP class of same name.
class Image {
    public:
        Image();
        Image(const std::string& name);
        Image(const std::string& name, int imageWidth, int imageHeight);
        
        void setFileName(const std::string& name) { filename = name; }
        void setWidth(int newWidth) { width = newWidth; }
        void setHeight(int newHeight) { height = newHeight; }
        
        std::string getFileName() const { return filename; }
        int getWidth() const { return width; }
        int getHeight() const { return height; }
    private:
        std::string filename;     //Output filename
        int width;                //Width of image
        int height;               //Height of image
};
} //end rt namespace

/**
 * Acceleration Structure
 */
class Scene; //forward declaration
class Space {
    public:
        Space();
        Space(int structureID);
        virtual ~Space();

        virtual void make(Scene& scn, int depth);
        virtual void print() const;
        virtual std::vector<Object*> intersect(const Ray& trace, double dmin, double dmax) const;

        int getID() { return id; }
    protected:
        int id;

    private:
        std::vector<Object*> objects;

};

class Scene {
    public:
        Scene();
        Scene(enum ProjType view);
        
        void init(); //< (re)initialize everything to default values
        //Parses the scene text file and fills out a SceneData struct
        int parseScene(char* file);
        float getPlaneDist() const;
        void clearPools();
        //void print();
        
        void setCamera(const Camera& cam) { camera = cam; }
        void setProjType(enum ProjType view) { proj = view; }
        void setImage(const rt::Image& image) { img = image; }
        void addVertex(const Vector& vert) { vertexPool.push_back(vert); }
        void addNormal(const Vector& norm) { normalPool.push_back(norm); }
        void addObject(Object* tri) { objs.push_back(tri); }
        void setBackground(const Vector& bg) { bgColor = bg; }
        void setMaxDepth(int maximumDepth) { maxDepth = maximumDepth; }
        void addLight(const Light& dirLight) { lights.push_back(dirLight); }
        void setAmbientLight(const Light& ambLight) { ambient = ambLight; }
        void setSpatialDepth(int depth) { sDepth = depth; }
        void setSpatialThreshold(int threshold) { sThresh = threshold; }
        void setSampleRate(int rate) { sampleNum = rate; }
        
        Camera getCamera() const { return camera; }
        enum ProjType getProjType() const { return proj; }
        rt::Image getImage() const { return img; }
        std::vector<Object*> getObjects() const { return objs; }
        std::vector<Sphere> getSpheres() const;
        std::vector<Triangle> getTriangles() const;
        Vector getBGColor() const { return bgColor; }
        int getDepth() const { return maxDepth; }
        std::vector<Light> getLights() const { return lights; }
        Light getAmbLight() const { return ambient; }
        int getNumObjects() const { return objs.size(); }
        int getSpatialDepth() const { return sDepth; }
        int getSpatialThreshold() const { return sThresh; }
        bool useSpaceStruct() const { return useSpStruct; }
        int getSampleRate() const { return sampleNum; }
        Space* getSpatialStruct() const { return spaceStruct; }
        
    private:
        std::vector<Vector> getVertices() { return vertexPool; }
        std::vector<Vector> getNormals() { return normalPool; }

        //Camera
        Camera camera;
        enum ProjType proj;
        //Image
        rt::Image img;
        //Objects (Spheres/Triangles)
        std::vector<Object*> objs;
        //Triangle (Pools are cleared after parsing for memory usage)
        std::vector<Vector> vertexPool;
        std::vector<Vector> normalPool;
        //Background
        Vector bgColor;
        //depth
        int maxDepth;
        //Lights
        std::vector<Light> lights;
        Light ambient;
        //Acceleration Structure
        int sDepth;
        int sThresh;
        bool useSpStruct;
        Space* spaceStruct;
        //Super Sample
        int sampleNum;
};






//Print functions used to print the contents of SceneData to test it
void printScene(Scene *scn);
void printCamera(Camera camera);
void printSphere(Sphere sph);
void printTriangle(Triangle tri);
void printMaterial(Material mat);
void printLight(Light l);
void printImage(rt::Image img);
void printVector(Vector fVec);

#endif

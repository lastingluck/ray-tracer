#include "parse.hpp"

class BVH : extends Space {
    public:
        BVH();
        BVH(int d);
        ~BVH();
        
        void print() const;
        AABB* getRoot() { return root; }
        void make(Scene& scn, int depth);
        std::vector<Object*> intersect(const Ray& trace, double dmin, double dmax) const;
        
    private:
        AABB* root;
        int depth;
        
        void printAABB(const AABB* b, int depth);
        bool intersect(const Ray& trace, const AABB* box, double dmin, double dmax) const;
        void findBoundingVerts(const Scene& scn, Vector& bl, Vector& tr) const;
        char findLongestAxis(const Vector& vmin, const Vector& vmax) const;
        void findMinMax(float x0, float x1, float x2, float& min, float& max) const;
};

class AABB {
    public:
        AABB();
        AABB(const Vector& minimum, const Vector& maximum);
        AABB(AABB* p);
        AABB(const std::vector<Sphere> sphs, const std::vector<Triangle> tris);
        
        void setMin(const Vector& m) { min = m; }
        void setMax(const Vector& m) { max = m; }
        void setLeftChild(AABB* child) { left = child; }
        void setRightChild(AABB* child) { right = child; }
        void setParent(AABB* p) { parent = p; }
        void addSphere(const Sphere& sph) { spheres.push_back(sph); }
        void addTriangle(const Triangle& tri) { triangles.push_back(tri); }
        void setSpheres(const std::vector<Sphere> sphs) { spheres = sphs; }
        void setTriangles(const std::vector<Triangle> tris) { triangles = tris; }
        
        Vector getMin() const { return min; }
        Vector getMax() const { return max; }
        std::vector<Sphere> getSpheres() const;
        std::vector<Triangle> getTriangles() const;
        bool isLeaf() const { return !left && !right; }
        AABB* getLeftChild() const { return left; }
        AABB* getRightChild() const { return right; }
        AABB* getParent() const { return parent; }
        int getObjectNum() const { return objects.size(); }
        
        bool isInBox(const Sphere& sph) const;
        bool isInBox(const Triangle& tri) const;
        bool isInBox(const Plane& pln) const;
    private:
        std::vector<Object*> objects;
        Vector min;
        Vector max;
        //May not actually be left and right in space.
        AABB* left;
        AABB* right;
        AABB* parent;
        
        void findMinMax(float x0, float x1, float x2, float& min, float& max) const;
};

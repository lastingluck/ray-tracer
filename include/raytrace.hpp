#ifndef RAYTRACE_H_
#define RAYTRACE_H_

#include "EasyBMP.h"
#include "image.h"
#include "pixel.h"
#include "parse.hpp"

class RayTrace {
    public:
        RayTrace();
        //Performs a ray trace and returns an image
        Image* rayTrace(Scene& scn);
        
    private:
        Space* spaceStruct;
    
        Vector ave(Vector v[], int num);
        //Recursive ray structure
        Vector evaluateRayTree(const Scene& scn, const Ray& ray, int depth) const;
        //Returns a ray calculated from the pixel position and the viewing angle
        Ray getRay(int x, int y, int w, int h, const Vector& p1, const Vector& p2, 
                   double pd, const Camera& c, ProjType proj, bool sample) const;
        //Returns a pixel with the background color
        Vector getColor(const Intersect* i, const Scene& scn, int depth) const;
        //Distance to the viewing plane
        double getPlaneDist(double angle, int h) const;
        //Finds 2 extreme points of the viewing plane
        void getExtremePoints(const Camera& c, double d, double w, double h, Vector& p1, Vector& p2) const;
        //Tests for intersections
        Intersect* intersect(const Ray& trace, const Scene& scn, double tmin, double tmax) const;
        void deleteSpatialStructure();
};

#endif
